<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiJourneysQualificationController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "journeys";        
				$this->permalink   = "journeys_qualification";    
				$this->method_type = "get";    
		    }
			
			/*
			public function hook_validate(&$postdata) {
				$employee_id = $postdata['employee_id'];
				$checCalificacion = DB::table('qualification')->where('employees_id',$employee_id)->first();;
			
				if($checkmail) {
				  $this->hook_api_message = 'Email already registered!';
				  $this->validate = true;
				}
			}
			*/
			
			public  $employees_id;
			
		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
				$this->$employees_id = $postdata["employee_id"];
				//dd($employees_id);
				
				//$calificacion = DB::table('qualification')->where('employees_id', $employees_id)->first();
				//dd($calificacion->journeys_id);
		    }

		    public function hook_query(&$query) {
				$query->where('end_date','<',date("Y-m-d") );
		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

				$datos = $result['data'];
				$new_data = array();
				
				
				foreach ($datos as &$valor) {
					
					$id_jornada = $valor->id;
					
					$checCalificacion = DB::table('qualification')->where('employees_id',$this->$employee_id)->where('journeys_id',$id_jornada)->first();
					if($checCalificacion) {
					}
					else{
						array_push($new_data, $valor );
					}
	
				}
				
				$result['data'] =  $new_data;

		    }

		}