<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiProgramsController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "programs";        
				$this->permalink   = "programs";    
				$this->method_type = "get";
				$this->orderby = "id,asc";
				//$table->increments('id');
	
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
				$postdata['orderby'] = "order_item,asc";
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query
				//dd($query);
				//$query->->orderBy('order_item', 'asc')

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}