<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiInsertExperienceController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "experiences";        
				$this->permalink   = "insert_experience";    
				$this->method_type = "get";    
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
				
				$this->hook_api_message  = "Se ha registrado la experiencia correctamente, en breve será revisada para su publicación";
				$this->validate = true;
		    }
			


		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process
				
				
				
				//$image = $postdata["base"];  // your base64 encoded
				

				/*
				$array_data= explode(",", $image);//partimos la informacion

				$tipo = explode(";", ($array_data[0]) ); //extraemos el tipo
				
				$array_ext = explode("/", $tipo[0]);
				
				
				$ext = $array_ext[1];
				$base = $array_data[1];
				*/
/*
				$image = str_replace('data:image/png;base64,', '', $image);
				$image = str_replace(' ', '+', $image);
				$imageName = str_random(10).'.'.'png';
				

				\File::put(storage_path(). '/app/uploads/resources/' . $imageName, base64_decode($image));

				$nombre = 'uploads/resources/'.$imageName;
				
*/
				//dd($postdata);
				
				//$row = CRUDBooster::first($this->table,$result['id']);
				//DB::table($this->table)->where('id', $result['id'])->update(['image' => $nombre ]);
				//dd($row);
		    }

		}