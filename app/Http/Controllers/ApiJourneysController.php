<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiJourneysController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "journeys";        
				$this->permalink   = "journeys";    
				$this->method_type = "get";    
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process

		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process
				
				setlocale(LC_ALL, 'es_ES');
				
				//dd( strftime("%d de %B", strtotime("2018-10-31") ) );
				
				$datos = $result['data'];
			
				
				$new_data = array();
				
				
				foreach ($datos as &$valor) {
					
					$valor->start_date = strftime("%d de %B", strtotime( $valor->start_date ) ) ;
					$valor->end_date = strftime("%d de %B", strtotime( $valor->end_date ) ) ;
					array_push($new_data, $valor );
					
	
				}
				
				$result['data'] =  $new_data;
				//dd($new_data);
				

		    }

		}