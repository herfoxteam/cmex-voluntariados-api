<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiInscriptionsController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "inscriptions";        
				$this->permalink   = "inscriptions";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
				
		        //This method will be execute before run the main process
				$employee_id = $postdata['employee_id'];
				$journey_id  = $postdata['journey_id'];
				$checkInscription = DB::table('inscriptions')->where('employee_id',$employee_id)->where('journey_id',$journey_id)->first();;
			
				if($checkInscription) {
				  $this->hook_api_message = 'ya está registrado a la jornada';
				  dd( 'ya está registrado a la jornada' );
				}

		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}