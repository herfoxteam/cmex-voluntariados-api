<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiRegistryTokenController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "token_push";        
				$this->permalink   = "registry_token";    
				$this->method_type = "get";    
		    }

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
				//dd($postdata);
				$token = $postdata['token'];
				$checktoken = DB::table('token_push')->where('token',$token)->first();;
			
				if($checktoken) {
				  $this->hook_api_message = 'este token ya está registrado';
				  //$postdata["employee_id"] = '';
				  dd( 'este token ya está registrado' );
				}

		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query
				//$query->where('token', '!=', '123');
				//dd($query);

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }
			
			

		}