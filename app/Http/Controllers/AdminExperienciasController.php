<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminExperienciasController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "title";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = false;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = true;
			$this->table = "experiences";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Nombre","name"=>"employee_id","join"=>"employees,name"];
			$this->col[] = ["label"=>"Apellidos","name"=>"employee_id","join"=>"employees,last_name"];
			$this->col[] = ["label"=>"No. Colaborador","name"=>"employee_id","join"=>"employees,no_employee"];
			$this->col[] = ["label"=>"No. Cédula","name"=>"employee_id","join"=>"employees,document"];
			$this->col[] = ["label"=>"No. Celular","name"=>"employee_id","join"=>"employees,cellphone"];
			$this->col[] = ["label"=>"Correo","name"=>"employee_id","join"=>"employees,email"];
			$this->col[] = ["label"=>"Foto","name"=>"employee_id","join"=>"employees,photo","image"=>true];
			
			$this->col[] = ["label"=>"Programa","name"=>"program_id","join"=>"programs,title"];
			$this->col[] = ["label"=>"Imágen","name"=>"image","image"=>true];
			$this->col[] = ["label"=>"Título","name"=>"title"];
			$this->col[] = ["label"=>"Lugar","name"=>"place"];
			$this->col[] = ["label"=>"Fecha","name"=>"date"];
			$this->col[] = ["label"=>"Me Gusta","name"=>"(select count(likes.id) from likes where likes.experience_id = experiences.id) as total_favorite"];
			$this->col[] = ["label"=>"Estado","name"=>"status","join"=>"status_experiences,title"];
			$this->col[] = ["label"=>"Activo","name"=>"active","join"=>"status_type,title"];
			//$this->col[] = ['label'=>'Aprobar','name'=>'status','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'status_experiences,title'];

			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Empleado','name'=>'employee_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'employees,name'];
			$this->form[] = ['label'=>'Programas','name'=>'program_id','type'=>'select2','validation'=>'required','width'=>'col-sm-10','datatable'=>'programs,title'];
			$this->form[] = ['label'=>'Imágen','name'=>'image','type'=>'upload','validation'=>'required|image|max:3000','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Imágen 2','name'=>'image_2','type'=>'upload','validation'=>'image|max:3000','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Imágen 3','name'=>'image_3','type'=>'upload','validation'=>'image|max:3000','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Imágen 4','name'=>'image_4','type'=>'upload','validation'=>'image|max:3000','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Imágen 5','name'=>'image_5','type'=>'upload','validation'=>'image|max:3000','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Título','name'=>'title','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Lugar','name'=>'place','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Fecha','name'=>'date','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Descripción','name'=>'description','type'=>'textarea','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Estado','name'=>'status','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'status_experiences,title'];
			
			
			
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Empleado','name'=>'employee_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'employees,name'];
			//$this->form[] = ['label'=>'Imagen','name'=>'image','type'=>'upload','validation'=>'required|image|max:3000','width'=>'col-sm-10','help'=>'File types support : JPG, JPEG, PNG, GIF, BMP'];
			//$this->form[] = ['label'=>'Titulo','name'=>'title','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			//$this->form[] = ['label'=>'Lugar','name'=>'place','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Fecha','name'=>'date','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Descripción','name'=>'description','type'=>'textarea','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();
			$this->sub_module[] = ['label'=>'Likes','path'=>'likes','parent_columns'=>'date','foreign_key'=>'experience_id','button_color'=>'success','button_icon'=>'fa fa-bars'];


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();
			$this->addaction[] = ['label'=>'Aprobar','url'=>CRUDBooster::mainpath('set-status/active/[id]'),'icon'=>'fa fa-check','color'=>'success','showIf'=>"[status] == '2'"];
			//$this->addaction[] = ['label'=>'Aprobar','url'=>CRUDBooster::mainpath('set-status/active/[id]'),'icon'=>'fa fa-check','color'=>'success','showIf'=>"[status] == '2'"];
			//$this->addaction[] = ['label'=>'Desaprobar','url'=>CRUDBooster::mainpath('set-status/pending/[id]'),'icon'=>'fa fa-ban','color'=>'warning','showIf'=>"[status] == '1'", 'confirmation' => true];
		


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();
			$this->button_selected[] = ['label'=>'Aprobar los seleccionados','icon'=>'fa fa-check','name'=>'set_active'];
			$this->button_selected[] = ['label'=>'Desaprobar los seleccionados','icon'=>'fa fa-close','name'=>'set_inactive'];
			
			$this->button_selected[] = ['label'=>'Activar los seleccionados','icon'=>'fa fa-check','name'=>'set_act'];
			$this->button_selected[] = ['label'=>'Desactivar los seleccionados','icon'=>'fa fa-close','name'=>'set_inac'];

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();
	

	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array(); 
			$this->table_row_color[] = ['condition'=>"[status] == '1'","color"=>"success"]; 
			$this->table_row_color[] = ['condition'=>"[status] == '2'","color"=>"warning"]; 
			$this->table_row_color[] = ['condition'=>"[status] == '3'","color"=>"secondary"];    	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }
		
		//PARA APROBACION UNO A UNO
		public function getSetStatus($status,$id) {
		   DB::table('experiences')->where('id',$id)->update(['status'=>1]);
		   
		   $experiencia_data = DB::table('experiences')->where('id',$id)->first();
		   
			$employee_data = DB::table('employees')->where('id',$experiencia_data->employee_id)->first();
		   
		   


		   $tokens = DB::table('token_push')->pluck('token');
				
				$msg = array(
					'body'  => $employee_data->name." ".$employee_data->last_name." ha compartido la expericiencia ".$experiencia_data->title ,
					'title' => "¡Nueva expericiencia!",
					'icon'  => 'myicon',/*Default Icon*/
					'sound' => 'mySound'/*Default sound*/
				);
				
				$data = array(
					'action_destination' => '#/experience_detail/'.$id
				);

				//MASIVO
				$fields = array(
					'registration_ids' => $tokens,
					'notification' => $msg,
					'data' => $data
				);
	
				$headers = array(
					'Authorization: key='.'AIzaSyBi5RA7YfCsLwzIrVX41_gnEYGnSTpvZvw',
					'Content-Type: application/json'
				);
				#Send Reponse To FireBase Server    
				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
				$result = curl_exec($ch );
				curl_close( $ch );
				#Echo Result Of FireBase Server
			
			CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"La experiencia a sido actualizada.","info");
		   
		}


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
		
		//PARA APROBACION MASIVA
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
			//$id_selected is an array of id 
			//$button_name is a name that you have set at button_selected 
		  
			if($button_name == 'set_active') {
				DB::table('experiences')->whereIn('id',$id_selected)->update(['status'=>'1']);
				
				//
				$tokens = DB::table('token_push')->pluck('token');
				
				$msg = array(
					'body'  => 'Hay nuevas experiencias publicadas. entérate de más',
					'title' => "¡Nueva expericiencia!",
					'icon'  => 'myicon',/*Default Icon*/
					'sound' => 'mySound'/*Default sound*/
				);
				
				$data = array(
					'action_destination' => '#/news/2'
				);
			
				//UNICO
				/*
				$fields = array(
					'to' => 'cyHa_qYelnM:APA91bFUuxb6SQ9Z7GR93mYxDh4f22Uqvhvob5nCxNO-cHyYoLAcGtHoCDhFHGQuB1caWTEw6LgKpPm-fdJmn2il8RBle5ItmYIvSNkdQlBmubmcOTpMZ2D_mEAotQjQCYd-iM7BGCN0',
					'notification' => $msg
				);
				*/
				
				//MASIVO
				$fields = array(
					'registration_ids' => $tokens,
					'notification' => $msg,
					'data' => $data
				);
	
				$headers = array(
					'Authorization: key='.'AIzaSyBi5RA7YfCsLwzIrVX41_gnEYGnSTpvZvw',
					'Content-Type: application/json'
				);
				#Send Reponse To FireBase Server    
				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
				$result = curl_exec($ch );
				curl_close( $ch );
				#Echo Result Of FireBase Server
				
				
			}
			
			if($button_name == 'set_inactive') {
				DB::table('experiences')->whereIn('id',$id_selected)->update(['status'=>'3']);
				
				//
				$tokens = DB::table('token_push')->pluck('token');

			}
			
			if($button_name == 'set_act') {
				DB::table('experiences')->whereIn('id',$id_selected)->update(['active'=>'1']);
				
				//
				$tokens = DB::table('token_push')->pluck('token');

			}
			
			if($button_name == 'set_inac') {
				DB::table('experiences')->whereIn('id',$id_selected)->update(['active'=>'2']);
				
				//
				$tokens = DB::table('token_push')->pluck('token');

			}
	            
	    }
		


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	
			
			//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {  
		
			
			
			

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here
			$employee_id = $postdata["employee_id"];
			$checkEmployee = DB::table('employees')->where('id',$employee_id)->first();
			
			if($postdata["status"] == 1){
				$mensaje = $checkEmployee->name.' '.$checkEmployee->last_name.' ha publicado una nueva experiencia, entérate de más';
				//dd($mensaje);

				
			
			}
			
			

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 


	}