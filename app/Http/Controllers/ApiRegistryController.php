<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiRegistryController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "employees";        
				$this->permalink   = "registry";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
				
				$id = $postdata['no_employee'];
				$no_employee = DB::table('employees')->where('no_employee',$id)->first();
			
				if($no_employee == null) {
					//dd("sin numero");
				  $this->hook_api_status = 3;
				  $this->hook_api_message  = "Lo sentimos, no tenemos este número de empleado registrado, si el problema persite, póngase en contacto con el administrador del programa.";
				  $this->validate = false;
				}
				
				else{
					$registro = DB::table('employees')->where('no_employee',$id);
					$registro->update([ 'name' => $postdata['name'],
					'last_name' => $postdata['last_name'],
					'photo' => $postdata['photo'],
					'document' => $postdata['document'],
					'cellphone' => $postdata['cellphone'],
					'email' => $postdata['email'],
					'age' => $postdata['age'],
					'description' => $postdata['description'],
					'location' => $postdata['location'],
					'shirt_size' => $postdata['shirt_size'],
					'shoe_size' => $postdata['shoe_size'],
					'status' => 1
					]);
					$this->hook_api_status = 1;
					$this->hook_api_message  = "Los datos han sido actualizados, ahora está registrado en el programa de voluntariado Cemex!";
					$this->validate = true;
					
				}


		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query
				//$postdata['no_employee'] = '123456789';
				
				
			
				

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process
				
				

		    }
			
			
		}